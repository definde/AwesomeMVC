<?php

namespace Dba\AwesomeMvc\Persistence\Storage;

use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;

class XmlStorageAdapter {

    protected $xmlFileName = NULL;

    protected $xmlDirectory = __DIR__ . "/../../../../../";

    protected $xmlReader;

    protected $configurationService;

    const CONFIG_PATH = 'persistence/xmlStorageAdapter';

    protected $configuration;

    public function __construct(ConfigurationService $configurationService)
    {
        $this->setConfigurationService($configurationService);
        $this->setConfiguration($this->getConfigurationService()->getConfigurationByPath(self::CONFIG_PATH));
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param mixed $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }


    /**
     * @return mixed
     */
    public function getConfigurationService()
    {
        return $this->configurationService;
    }

    /**
     * @param mixed $configurationService
     */
    public function setConfigurationService($configurationService)
    {
        $this->configurationService = $configurationService;
    }



    /**
     * @return mixed
     */
    public function getXmlFileName()
    {
        if(NULL == $this->xmlFileName){
            $this->setXmlFileName($this->getConfiguration()['xmlFileName']);
        }
        return $this->xmlFileName;
    }

    /**
     * @param mixed $xmlFileName
     */
    public function setXmlFileName($xmlFileName)
    {
        $this->xmlFileName = $xmlFileName;
    }

    /**
     * @return mixed
     */
    public function getXmlDirectory()
    {
        return $this->xmlDirectory;
    }

    /**
     * @param mixed $xmlDirectory
     */
    public function setXmlDirectory($xmlDirectory)
    {
        $this->xmlDirectory = $xmlDirectory;
    }

    /**
     * @return mixed
     */
    public function getXmlReader()
    {
        return $this->xmlReader;
    }

    /**
     * @param mixed $xmlReader
     */
    public function setXmlReader($xmlReader)
    {
        $this->xmlReader = $xmlReader;
    }

    /**
     *
     * @return mixed
     */
    public function loadFileNameFromConfiguration(){
        return $this->getConfigurationService()->getConfigurationByPath('persistence/xmlStorageAdapter/xmlFileName');
    }


    public function findAll(){
        $completeFileName = $this->getConfigurationService()->getApp()->getDocumentRoot(). $this->getXmlFileName();
        if (file_exists( $completeFileName)) {
            $dataToMap = simplexml_load_file($completeFileName);
        } else {
            die('file not exist');
        }

        return $dataToMap;
    }
}