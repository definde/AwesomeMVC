<?php


namespace Dba\AwesomeMvc\Persistence\DataMapper\EntityMapper;

interface EntityMapperInterface {

    public function createEntityFromRow($row);
}