<?php

namespace Dba\AwesomeMvc\Persistence\DataMapper;

interface DataMapperInterface {

    public function findAll();
    public function createEntityFromRow($row);
}