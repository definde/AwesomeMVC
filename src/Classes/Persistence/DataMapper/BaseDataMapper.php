<?php

namespace Dba\AwesomeMvc\Persistence\DataMapper;

use Dba\AwesomeMvc\Persistence\DataMapper\DataMapperInterface;
use Dba\AwesomeMvc\Persistence\DataMapper\EntityMapper\EntityMapperInterface;
use Dba\AwesomeMvc\Persistence\Storage\XmlStorageAdapter;

class BaseDataMapper implements DataMapperInterface {

    /**
     * Contains the object to map from an storage to a concrete entity.
     * @var EntityMapperInterface
     */
    protected $entityMapper;

    /**
     *  Contains the storageadapter. Might be a database, xml, simple file or something else.
     * @var XmlStorageAdapter;
     */
    protected $storageAdapter;


    public function __construct(XmlStorageAdapter $storageAdapter, EntityMapperInterface $entityMapper)
    {
        $this->setEntityMapper($entityMapper);
        $this->setStorageAdapter($storageAdapter);
    }


    /**
     *
     * @return \Dba\AwesomeMvc\Persistence\Storage\XmlStorageAdapter
     */
    public function getStorageAdapter()
    {
        return $this->storageAdapter;
    }

    /**
     * @return mixed
     */
    public function getEntityMapper()
    {
        return $this->entityMapper;
    }

    /**
     * @param mixed $entityMapper
     */
    public function setEntityMapper( EntityMapperInterface $entityMapper)
    {
        $this->entityMapper = $entityMapper;
    }


    /**
     *
     * @param \Dba\AwesomeMvc\Persistence\Storage\XmlStorageAdapter $storageAdapter
     */
    public function setStorageAdapter(XmlStorageAdapter $storageAdapter)
    {
        $this->storageAdapter = $storageAdapter;
    }


    /**
     *  Fetches all rows from the storage an maps it to entities.
     * @return array|null
     */
    public function mapAll()
    {
        $entities = array();
        $data = $this->getStorageAdapter()->findAll();

        foreach ($data as $row) {
            $entities[] = $this->createEntityFromRow($row);
        }

        return $entities;
    }

    /**
     * Just a little proxy to map all...
     */
    public function findAll()
    {
        return $this->mapAll();
    }

    /**
     * Creates the concrete entity with the configured adapter.
     *
     * @param $row
     * @return mixed
     */
    public function createEntityFromRow($row)
    {
        return $this->getEntityMapper()->createEntityFromRow($row);
    }
}