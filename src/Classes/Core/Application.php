<?php


namespace Dba\AwesomeMvc\Core;

use Dba\AwesomeMvc\Mvc\DispatcherInterface;
use Dba\AwesomeMvc\Mvc\Request\CliRequest;
use Dba\AwesomeMvc\Mvc\Request\RequestInterface;
use Dba\AwesomeMvc\Mvc\Request\RequestParserFactory;
use Dba\AwesomeMvc\Mvc\Response\ResponseInterface;
use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;
use DI\ContainerBuilder;
use Dba\WorldOfMusic\Controller\AlbumController;
/**
 * A dispatcher class to resolve command line commands.
 *
 * Class CliDispatcher
 * @package Dba\AwesomeMvc\Mvc\Cli
 */
class Application implements DispatcherInterface
{

    /**
     * @var \Dba\AwesomeMvc\Mvc\Request\RequestInterface
     */
    protected $request;
    protected $controller;
    protected $action;
    protected $module;
    protected $response;
    protected $configurationService;
    protected $container;
    protected $documentRoot;

    /**
     * @return mixed
     */
    public function getConfigurationService()
    {
        return $this->configurationService;
    }

    /**
     * @param mixed $configurationService
     */
    public function setConfigurationService($configurationService)
    {
        $this->configurationService = $configurationService;
    }


    /**
     * @return mixed
     */
    public function getDocumentRoot()
    {
        return $this->documentRoot;
    }

    /**
     * @param mixed $documentRoot
     */
    public function setDocumentRoot($documentRoot)
    {
        $this->documentRoot = $documentRoot;
    }



    /**
     * @return mixed
     */
    public function getContainer()
    {
        if(NULL == $this->container){
            $this->setContainer($this->loadDiContainer());
        }
        return $this->container;
    }

    private function loadDiContainer(){
        $container = ContainerBuilder::buildDevContainer();
        return $container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }


    public function get($param){
        return $this->getContainer()->get($param);
    }

    public function make($name, array $parameters = []){
        return $this->getContainer()->make($name, $parameters);
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param mixed $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }


    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param RequestInterface $request
     */
    public function setRequest(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse(ResponseInterface $response)
    {
        $this->response = $response;
    }


    public function parseRequest(RequestInterface $request)
    {
        $parser = RequestParserFactory::createRequestParser($request);
        $parser->parseRequest($request, $this);
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param mixed $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action . 'Action';
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action ;
    }

    /**
     * Resolve the classname of the requested controller class.
     *
     * @return mixed
     */
    public function resolveClassName(){
        $defaultNamespace = "Dba";
        $module = $this->getModule();
        $controller = $this->getController();

        $className = '\\' . $defaultNamespace . '\\' . $module . '\\' . 'Controller' . '\\' . $controller . 'Controller';
        if(false == class_exists($className)){
            throw new \Exception('The requested route could not be dispatched. Class ' . $className . ' does not exist');
        }
        return new $className($this->getRequest(), $this);
    }

    /**
     * Parse the request and execute the requested controller action.
     * @param RequestInterface $request
     */
    public function run(RequestInterface $request)
    {
        $this->setConfigurationService($this->getContainer()->get(ConfigurationService::class));
        $this->getConfigurationService()->setApp($this);
        $this->getConfigurationService()->initConfiguration();
        $this->setRequest($request);
        $this->parseRequest($request);

        call_user_func_array(array($this->resolveClassName(), $this->getAction()), $request->getParameters());
    }

}