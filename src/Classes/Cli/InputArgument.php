<?php
namespace Dba\AwesomeMvc\Mvc\Cli;

class InputArgument {

    protected $argv;

    /**
     * @return mixed
     */
    public function getArgv()
    {
        return $this->argv;
    }

    /**
     * @param mixed $argv
     */
    public function setArgv($argv)
    {
        $this->argv = $argv;
    }


}