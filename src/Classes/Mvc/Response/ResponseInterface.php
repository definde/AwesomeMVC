<?php

namespace Dba\AwesomeMvc\Mvc\Response;

interface ResponseInterface {

    public function getParameters();
    public function setParameters();
    public function getHeader();
    public function getParam($name);

}