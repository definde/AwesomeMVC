<?php
namespace Dba\AwesomeMvc\Mvc\Request;


use Dba\AwesomeMvc\Mvc\Request\RequestInterface;

abstract class AbstractRequest implements RequestInterface {
    const REQUEST_TYPE_CLI = 'cli';
    const REQUEST_TYPE_HTTP = 'http';

    protected $requestType;
    protected $globalRequest;

    /**
     * @return mixed
     */
    public function getGlobalRequest()
    {
        return $this->globalRequest;
    }

    /**
     * @param mixed $globalRequest
     */
    public function setGlobalRequest($globalRequest)
    {
        $this->globalRequest = $globalRequest;
    }


    /**
     *
     * @return mixed
     */
    public function getRequestType()
    {
        return $this->requestType;
    }

    /**
     *
     * @param mixed $requestType
     */
    public function setRequestType($requestType)
    {
        $this->requestType = $requestType;
    }


}