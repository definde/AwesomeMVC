<?php

namespace Dba\AwesomeMvc\Mvc\Request;

use Dba\AwesomeMvc\Mvc\Request\RequestInterface;

class RequestParserFactory {

    static public function createRequestParser(RequestInterface $request){
        $parserType = $request->getRequestType();
        $className = "\\Dba\\AwesomeMvc\\Mvc\\Request\\" . ucfirst($parserType) . "RequestParser";

        if(class_exists($className)){
            return new $className($request);
        }
    }
}