<?php

namespace Dba\AwesomeMvc\Mvc\Request;


use Dba\AwesomeMvc\Mvc\Request;

/**
 * Class CliRequest
 * @package Dba\AwesomeMvc\Mvc\Request
 */
class CliRequest extends AbstractRequest implements RequestInterface
{
    protected $parameters = array();

    protected $requestType = self::REQUEST_TYPE_CLI;

    protected $defaultRouteOrder = ['module', 'controller', 'action'];

    protected $routeParameter;

    /**
     * @return mixed
     */
    public function getRouteParameter()
    {
        return $this->routeParameter;
    }

    /**
     * @param mixed $routeParameter
     */
    public function setRouteParameter($routeParameter)
    {
        $this->routeParameter = $routeParameter;
    }


    /**
     * @return string
     */
    public function getRequestType()
    {
        return $this->requestType;
    }

    /**
     * @param string $requestType
     */
    public function setRequestType($requestType)
    {
        $this->requestType = $requestType;
    }

    /**
     * @return array
     */
    public function getDefaultRouteOrder()
    {
        return $this->defaultRouteOrder;
    }

    /**
     * @param array $defaultRouteOrder
     */
    public function setDefaultRouteOrder($defaultRouteOrder)
    {
        $this->defaultRouteOrder = $defaultRouteOrder;
    }

    public function __construct($globalRequest){
        $this->setGlobalRequest($globalRequest);

        $this->setRouteParameter($globalRequest['argv'][1]);

        $this->resolveParameters($globalRequest['argv']);
    }
    /**
     * @return mixed
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param mixed $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }


    /**
     *
     */
    public function getHeader()
    {
        // TODO: Implement getHeader() method.
    }

    /**
     * @param $name
     */
    public function getParam($name)
    {
        $params = $this->getParameters();

        if (isset($params[$name])) {
            return $params[$name];
        }
    }

    /**
     * Simple split every argument after the route to a key value pair.
     * @param $argv
     */
    private function resolveParameters($argv)
    {
        $param = [];
        unset($argv[0]);
        unset($argv[1]);
        if(count($argv) > 0){
            $iteration = 0;
            $key = NULL;
            foreach($argv as $arg){
                $iteration++;

                if(($iteration %2) == 1){
                    $key = $arg;
                } else {
                    $param[$key] = $arg;
                }
            }
        }
        $this->setParameters($param);
    }


}