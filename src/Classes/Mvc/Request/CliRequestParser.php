<?php


namespace Dba\AwesomeMvc\Mvc\Request;

use Dba\AwesomeMvc\Mvc\DispatcherInterface;
use Dba\AwesomeMvc\Mvc\Request\RequestInterface;

class CliRequestParser {

    protected $request;
    protected $dispatcher;


    public function parseRequest($request, $application){
        $this->setRequest($request);
        $this->resolveControllerAction($application);
    }

    protected function resolveControllerAction($application){
        $request = $this->getRequest();
        $defaultRouteOrder = $request->getDefaultRouteOrder();

        $requestParts = explode(':', $request->getRouteParameter());

        if(count($requestParts) == 3){
            foreach($defaultRouteOrder as $routePart){
                $setter = "set" . ucfirst($routePart);
                $application->$setter(array_shift($requestParts));
            }
        }
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }
}