<?php

namespace Dba\AwesomeMvc\Mvc\Request;

interface RequestInterface {

    public function getGlobalRequest();
    public function setGlobalRequest($request);

    public function getParameters();
    public function setParameters($parameters);
    public function getHeader();
    public function getParam($name);
    public function setRequestType($requestType);
    public function getRequestType();

}