<?php

namespace Dba\AwesomeMvc\Mvc\Controller;


use Dba\AwesomeMvc\Core\Application;
use Dba\AwesomeMvc\Mvc\Request\RequestInterface;

class BaseController {

    protected $app;

    protected $request;

    /**
     * @return mixed
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * @param mixed $app
     */
    public function setApp($app)
    {
        $this->app = $app;
    }



    public function __construct(RequestInterface $request, Application $app){
        $this->setApp($app);
        $this->setRequest($request);
    }

    public function getParam($paramName)
    {
        return $this->getRequest()->getParam($paramName);
    }

    public function get($param){
        return $this->getApp()->get($param);
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

}