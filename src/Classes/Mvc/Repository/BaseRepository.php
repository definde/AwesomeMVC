<?php

namespace Dba\AwesomeMvc\Mvc\Repository;

use Dba\WorldOfMusic\Domain\Model\Entity\Mapper\AlbumDataMapper;
use Dba\AwesomeMvc\Persistence\DataMapper\DataMapperInterface;
use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;

/**
 * Class BaseRepository
 * @package Dba\AwesomeMvc\Mvc\Repository
 */
class BaseRepository {


    /**
     * @var ConfigurationService
     */
    protected $configurationService;


    /**
     * @var DataMapperInterface
     */
    protected $dataMapper;

    public function __construct(AlbumDataMapper $dataMapper){
        $this->setDataMapper($dataMapper);
    }

    /**
     * @return ConfigurationService
     */
    public function getConfigurationService()
    {
        return $this->configurationService;
    }

    /**
     * @param ConfigurationService $configurationService
     */
    public function setConfigurationService($configurationService)
    {
        $this->configurationService = $configurationService;
    }


    /**
     * @return DataMapperInterface
     */
    public function getDataMapper()
    {
        return $this->dataMapper;
    }

    /**
     * @param DataMapperInterface $dataMapper
     */
    public function setDataMapper($dataMapper)
    {
        $this->dataMapper = $dataMapper;
    }
}