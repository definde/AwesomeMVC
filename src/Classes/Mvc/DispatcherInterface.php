<?php

namespace Dba\AwesomeMvc\Mvc;

use Dba\AwesomeMvc\Mvc\Request\RequestInterface;
use Dba\AwesomeMvc\Mvc\Response\ResponseInterface;

interface DispatcherInterface
{

    public function setRequest(RequestInterface $request);

    public function getRequest();

    public function getResponse();

    public function setResponse(ResponseInterface $response);

    public function setController($controller);
    public function setAction($action);

    public function run(RequestInterface $request);
}