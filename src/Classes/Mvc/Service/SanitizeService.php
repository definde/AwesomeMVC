<?php

namespace Dba\AwesomeMvc\Mvc\Service;
use Dba\AwesomeMvc\Core\Application;
use Symfony\Component\Yaml\Yaml;

class SanitizeService extends BaseService {

    public function htmlSpecialChars($string){
        return htmlspecialchars($string);
    }
}