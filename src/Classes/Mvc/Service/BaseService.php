<?php

namespace Dba\AwesomeMvc\Mvc\Service;


use Dba\AwesomeMvc\Mvc\Service\ConfigurationService;

class BaseService
{
    /**
     * @var ConfigurationService
     */
    protected $configurationService;

    public function __construct(ConfigurationService $service)
    {
        $this->setConfigurationService($service);
    }

    /**
     * @return mixed
     */
    public function getConfigurationService()
    {
        return $this->configurationService;
    }

    /**
     * @param mixed $configurationService
     */
    public function setConfigurationService(ConfigurationService $configurationService)
    {
        $this->configurationService = $configurationService;
    }

}