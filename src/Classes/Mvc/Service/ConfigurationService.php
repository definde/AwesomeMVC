<?php

namespace Dba\AwesomeMvc\Mvc\Service;
use Dba\AwesomeMvc\Core\Application;
use Symfony\Component\Yaml\Yaml;

class ConfigurationService {
    protected $configuration;

    protected $app;


    /**
     * @return mixed
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * @param mixed $app
     */
    public function setApp($app)
    {
        $this->app = $app;
    }


    public function getConfigurationByPath($path){
        $config = $this->getConfiguration();
        $pathArray = explode('/', $path);

        foreach($pathArray as $key => $keyName) {
            if (count($pathArray) > $key) {
                $config = $config[$keyName];
            }
        }

        return $config;
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param mixed $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }


    /**
     *
     */
    public function initConfiguration(){
        $value = Yaml::parse(file_get_contents($this->getApp()->getDocumentRoot() . 'application/config/config.yaml'));
        $this->setConfiguration($value);
    }
}